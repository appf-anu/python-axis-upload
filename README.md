# python-axis-upload

flask powered axis http uploader.

So many strange hacks:

* AXIS cameras do not do login with http except to "verify" the upload, so usse the "token" url parameter instead
* AXIS cameras do not use a normal file upload, they send the image data as an attachment with a "Content-Disposition" header
* AXIS cameras wont allow the path separator ("/") in the create folder field of an action rule with a http recipient, so replace with `$sep`
* AXIS cameras use a strcat of the create folder field and the base file name field with the "/" character


when you set up the http recipient, set the url to _http://`camera_name`.`camera_group`.upload.domain.com/upload?token=`token`_


our format for the Create Folder field in the Action rule setup is normally:

`#n/%Y/%Y_%m/%Y_%m_%d/%Y_%m_%d_%H/`

however for this it should be:

`#n$sep%Y$sep%Y_%m$sep%Y_%m_%d$sep%Y_%m_%d_%H`


base file name should remain:

`#n_%Y_%m_%d_%H_%M_00_00.jpg`


