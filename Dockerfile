FROM python:3.8.3-alpine

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED=1
ENV UPLOAD_FOLDER="/data"
ENV TOKEN=""

EXPOSE 8080
COPY . .

CMD ["waitress-serve", "--port=8080", "--max-request-body-size=53687091200", "run:app"]