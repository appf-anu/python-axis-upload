#!/usr/bin/env python3
import os
from waitress import serve
from flask import Flask, request
import datetime


def printl(*args, **kwargs):
    return print(datetime.datetime.now().isoformat(), "-", *args, **kwargs)


UPLOAD_FOLDER = os.environ.get("UPLOAD_FOLDER", '/data')

TOKEN = os.environ.get("TOKEN")
if not TOKEN:
    import binascii
    TOKEN = binascii.hexlify(os.urandom(16)).decode('utf-8')

ALLOWED_CONTENT_TYPES = {'image/png', 'image/jpeg'}

print(f'token={TOKEN}')
print(f'UPLOAD_FOLDER={UPLOAD_FOLDER}')
print(f'allowed content-types: {",".join(ALLOWED_CONTENT_TYPES)}')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = (2**30) * 50


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    request_token = request.args.get("token")
    if not request_token:
        return "no access token", 401
    if type(request_token) is str:
        request_token = request_token.replace("?undefined", "")
    if request_token != TOKEN:
        printl(f"denied access -> {request.remote_addr}\ttoken={request_token}")
        return "Access Denied", 401
    if request.method == 'POST':
        if request.content_type not in ALLOWED_CONTENT_TYPES:
            return f'Content-Type is not allowed {",".join(ALLOWED_CONTENT_TYPES)}', 400
        cdisposition_header = request.headers.get('Content-Disposition')
        if not cdisposition_header:
            return 'provide Content-Disposition header', 400
        filename = cdisposition_header.replace("attachment; ", "").replace('filename=', "").replace('"', '')
        filename = filename.replace("$sep", "/")
        filename = filename.replace("..", "")  # not today
        filename = filename.replace("//", "/")
        filename = os.path.join(UPLOAD_FOLDER, filename)

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'wb') as f:
            f.write(request.data)
        printl(f"wrote image {request.remote_addr} -> {filename}")
        return 'ok', 200
    if request.method == 'GET':
        printl(f"GET -> {request.remote_addr}")
        return 'ok', 200
    return 'ok', 200
